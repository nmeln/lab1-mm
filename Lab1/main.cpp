#include <iostream>
#include "header.h"


int main() {
	std::cout << "Enter number of quations" << std::endl;
	int numb;
	std::cin >> numb;

	LinProg lp(numb);

	lp.set_coefficients();
	lp.find_intersection();
	std::cout << "Max = " << lp.GetMax() << std::endl;
	std::cout << "Min = " << lp.GetMin() << std::endl;

	return 0;
}