class LinProg{

public:
	LinProg(int temp);
	~LinProg();
	void set_coefficients();
	void find_intersection();
	int check_point_validity(double x, double y);
	double Func(double x, double y);
	double GetMax();
	double GetMin();

private:
	double **equationsArr;
	double func[2];
	int equationsNumber;
	double max, min;
};
