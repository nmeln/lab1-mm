#include "header.h"
#include <iostream>
#include <cfloat>

LinProg::LinProg(int quantity){
	max = DBL_MIN;
	min = DBL_MAX;
	equationsNumber = quantity;
	equationsArr = new double*[quantity];
	for (int i = 0; i < quantity; i++)
		equationsArr[i] = new double[4];
}

LinProg::~LinProg(){
	for (int i = 0; i < equationsNumber; i++)
		delete[] equationsArr[i];
	delete[] equationsArr;
}

void LinProg::set_coefficients(){
	for (int i = 0; i<equationsNumber; i++){
		std::cout << "Enter coefficients for " << i + 1 << " equation" << std::endl;
		std::cin >> equationsArr[i][0] >> equationsArr[i][1] >> equationsArr[i][2];  // 7x1 + 5x2  >=  7, ������ ����� 7 5 7
		std::cout << "Enter if it's <= (0) or >= (1)" << std::endl; //���� ����� ���������
		std::cin >> equationsArr[i][3]; // 0, �� <=
	}
	std::cout << "Enter your formula" << std::endl;
	std::cin >> func[0] >> func[1]; //����� �������, ��� ������� ����. �� ����������
}

void LinProg::find_intersection(){
	//������� ������

	double *tempEquation, x, y;

	for (int i = 0; i < equationsNumber; i++){
		//���������
		tempEquation = equationsArr[i]; //��� ����� ����� �� �������� �� ���� ������ �������
		for (int j = 0; j < equationsNumber; j++){
			if (j != i){
				
				x = (tempEquation[2] * equationsArr[j][1] - equationsArr[j][2] * tempEquation[1]) / 
					(tempEquation[0] * equationsArr[j][1] - equationsArr[j][0] * tempEquation[1]);

				y = (tempEquation[0] * equationsArr[j][2] - equationsArr[j][0] * tempEquation[2]) / 
					(tempEquation[0] * equationsArr[j][1] - equationsArr[j][0] * tempEquation[1]);

				if (check_point_validity(x, y)){
					//���� ����� ���������� ���������
					if (Func(x, y) > max)
						max = Func(x, y); //� ����� ����������
					if (Func(x, y) < min)
						min = Func(x, y); //� ����� ��������
				}
			}
		}

		//�������� ����� �� ����
		if (tempEquation[0] != 0){
			x = tempEquation[2] / tempEquation[0];
			if (check_point_validity(x, 0)){
				if (Func(x, 0) > max)
					max = Func(x, 0);
				if (Func(x, 0) < min)
					min = Func(x, 0);
			}
		}

		if (tempEquation[1] != 0){
			y = tempEquation[2] / tempEquation[1];
			if (check_point_validity(0, y)){
				if (Func(0, y) > max)
					max = Func(0, y);
				if (Func(0, y) < min)
					min = Func(0, y);
			}
		}
	}
}
int LinProg::check_point_validity(double x, double y){
	//��������, �� ����� �������� �� �����
	if (!(x >= 0 && y >= 0)) //���� �� ������ ��������
		return 0;

	for (int i = 0; i < equationsNumber; i++){
		if (equationsArr[i][3] == 0){ // ���� <=
			if (!(equationsArr[i][0] * x + equationsArr[i][1] * y <= equationsArr[i][2]))
				return 0;
		}
		else // ���� >=
			if (!(equationsArr[i][0] * x + equationsArr[i][1] * y >= equationsArr[i][2]))
				return 0;
	}
	return 1;
}

double LinProg::Func(double x, double y){
	return func[0] * x + func[1] * y;
}

double LinProg::GetMax(){
	return max;
}

double LinProg::GetMin(){
	return min;
}